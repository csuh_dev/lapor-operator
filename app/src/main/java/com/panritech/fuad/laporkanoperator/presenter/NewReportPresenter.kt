package com.panritech.fuad.laporkanoperator.presenter

import com.panritech.fuad.laporkanoperator.fragment.NewReportItemFragment
import com.panritech.fuad.laporkanoperator.model.ReportItem

class NewReportPresenter (private val reportView: NewReportItemFragment){

    fun getReportList(data: List<ReportItem>){
        reportView.showProgressBar()
        reportView.showReportItem(data)
    }
}