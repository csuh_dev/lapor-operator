package com.panritech.fuad.laporkanoperator.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.panritech.fuad.laporkanoperator.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PROSES_DATE = "prosesDate"
private const val ARG_PROSES_DESCRIPTION = "prosesDescription"
private const val ARG_PROSES_IMG = "prosesImg"

class ReportProcessFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var prosesDate: String? = null
    private var prosesDescription: String? = null
    private var prosesImg: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            prosesDate = it.getString(ARG_PROSES_DATE)
            prosesDescription = it.getString(ARG_PROSES_DESCRIPTION)
            prosesImg = it.getString(ARG_PROSES_IMG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_process, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance(prosesDate: String, prosesDescription: String, prosesImg: String) =
                ReportProcessFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PROSES_DATE, prosesDate)
                        putString(ARG_PROSES_DESCRIPTION, prosesDescription)
                        putString(ARG_PROSES_IMG, prosesImg)
                    }
                }
    }
}
