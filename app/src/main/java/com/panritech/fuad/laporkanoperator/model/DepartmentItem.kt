package com.panritech.fuad.laporkanoperator.model

data class DepartmentItem (
        var key: String = "",
        var name: String = "",
        var description: String = ""
)