package com.panritech.fuad.laporkanoperator.presenter

import com.panritech.fuad.laporkanoperator.model.DepartmentItem
import com.panritech.fuad.laporkanoperator.view.DepartmentListView
import com.panritech.fuad.laporkanoperator.view.ProgressBarView

class DepartmentListPresenter(private val departmentListView: DepartmentListView, private val progressBarView: ProgressBarView) {
    fun getDepartmentList(data: List<DepartmentItem>){
        progressBarView.showProgressBar()
        progressBarView.hideProgressBar()
        departmentListView.showDepartmentList(data)
    }
}