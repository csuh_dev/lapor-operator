package com.panritech.fuad.laporkanoperator.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.model.UserItem
import kotlinx.android.synthetic.main.activity_add_operator.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick

class AddOperatorActivity : AppCompatActivity() {

    private lateinit var newUserEmail: EditText
    private lateinit var newUserPassword: EditText
    private lateinit var newUserConfirmPass: EditText
    private lateinit var btnRegister: Button
    private lateinit var operatorValidationPassword: EditText
    private lateinit var auth: FirebaseAuth
    private lateinit var myRef: DatabaseReference
    private var user: MutableList<UserItem> = mutableListOf()
    private var operatorEmail: String = ""
    private var operatorPass: String = ""
    private var newUserPass: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_operator)

        auth = FirebaseAuth.getInstance()
        myRef = FirebaseDatabase.getInstance().reference

        newUserEmail = findViewById(R.id.txt_email)
        newUserPassword = findViewById(R.id.txt_password)
        newUserConfirmPass = findViewById(R.id.txt_confirm_password)
        btnRegister = findViewById(R.id.btn_register)

        btnRegister.onClick {
            validateForm()
        }
    }

    private fun validateForm() {
        if (newUserEmail.text.isNotEmpty() && newUserPassword.text.isNotEmpty() && newUserConfirmPass.text.isNotEmpty()) {
            if (newUserPassword.text.length > 5) {
                if ("${newUserPassword.text}" != "${newUserConfirmPass.text}") {
                    newUserPassword.backgroundResource = R.drawable.card_stroke_red
                    newUserConfirmPass.backgroundResource = R.drawable.card_stroke_red
                    snackbar(root_view, "Konfirmasi Password Tidak Cocok")
                } else {
                    alert("Apakah Data Yang Anda isi Sudah Benar?", "Tambah Operator") {
                        yesButton {
                            newUserPass = newUserPassword.text.toString()
                            val newUserItem = UserItem("", "${newUserEmail.text}", "", "", "", "", "", "operator", "", "active")
                            user.add(newUserItem)
                            validateOperator()
                        }
                        noButton {
                            it.cancel()
                        }
                    }.show()
                }
            } else {
                snackbar(root_view, "Password Harus Lebih dari 5 Karakter")
            }
        } else {
            snackbar(root_view, "Isi Semua Data")
        }
    }

    private fun validateOperator() {
        if (operatorPass != "")
            toast("")
        else {
            alert("Silahkan Memasukkan Password Anda", "Validasi Operator") {
                customView {
                    verticalLayout {
                        operatorValidationPassword = editText {
                            hint = "Password"
                            inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                        }.lparams(width = matchParent) {
                            leftMargin = dip(10)
                            rightMargin = dip(10)
                        }
                    }
                }
                okButton {
                    operatorPass = operatorValidationPassword.text.toString()
                    verifyOperator()
                }
                cancelButton {
                    finishAffinity()
                    FirebaseAuth.getInstance().signOut()
                    startActivity<LoginActivity>()
                    toast("Silahkan Login Kembali")
                }
            }.show()
        }
    }

    private fun verifyOperator() {
        val dialog = progressDialog(message = "Mohon Tunggu", title = "Memverifikasi Akun")
        dialog.show()
        dialog.progress = 30
        operatorEmail = auth.currentUser?.email.toString()
        Log.e("validation", operatorEmail)
        auth.signInWithEmailAndPassword(operatorEmail, operatorPass)
                .addOnCompleteListener(this) { task ->
                    dialog.progress = 80
                    if (task.isSuccessful) {
                        addUser()
                        dialog.progress = 100
                        toast("Berhasil Verifikasi")
                    } else {
                        toast("Anda Gagal Divalidasi")
                        validateOperator()
                    }
                    dialog.cancel()
                }

    }

    private fun addUser() {
        val dialog = progressDialog(message = "Mohon Tunggu", title = "Mendaftarkan Akun")
        dialog.show()
        dialog.progress = 50
        auth.createUserWithEmailAndPassword(user[0].email, newUserPass)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("createUser", "createUserWithEmail:success")
                        val userAuth = auth.currentUser
                        user[0].uuid = userAuth!!.uid
                        snackbar(root_view, "Berhasil Menambahkan Akun")

                        addUserDetail(user[0])
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("createUser", "createUserWithEmail:failure", task.exception)
                        toast("Authentication failed.")
                        snackbar(root_view, "Gagal Menambah Akun")
                    }
                    FirebaseAuth.getInstance().signOut()
                }

        dialog.progress = 80
        auth.signInWithEmailAndPassword(operatorEmail, operatorPass)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        dialog.progress = 100
                        dialog.cancel()
                    }
                }
    }

    private fun addUserDetail(userItem: UserItem) {
        myRef.child("users/operator/${userItem.uuid}/uuid").setValue(userItem.uuid)
    }
}
