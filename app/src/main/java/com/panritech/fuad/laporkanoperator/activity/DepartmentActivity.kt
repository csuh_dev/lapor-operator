package com.panritech.fuad.laporkanoperator.activity

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.google.firebase.database.*
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.adapter.MyDepartmentItemRecyclerViewAdapter
import com.panritech.fuad.laporkanoperator.model.DepartmentItem
import com.panritech.fuad.laporkanoperator.presenter.DepartmentListPresenter
import com.panritech.fuad.laporkanoperator.view.DepartmentListView
import com.panritech.fuad.laporkanoperator.view.ProgressBarView
import kotlinx.android.synthetic.main.activity_department.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.onRefresh

class DepartmentActivity : AppCompatActivity(), DepartmentListView, ProgressBarView {
    override fun showDepartmentDescription(data: DepartmentItem) {
        alert("${data.description} \n Apakah anda ingin mengedit komisi?",
                data.name) {
            yesButton {
                alertUpdateDepartment(data.key, data.name, data.description)
                it.cancel()
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    override fun deleteDepartment(data: DepartmentItem) {
        alert("Ingin Menghapus Komisi ${data.name}?") {
            yesButton { dialog ->
                myRef.child(data.key).removeValue().addOnSuccessListener {
                    snackbar(root_view, "Komisi ${data.name} dihapus")
                    dialog.cancel()
                }
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showDepartmentList(data: List<DepartmentItem>) {
        Log.e("departmentList", "$data")
        Log.e("departmentList", "$department")
        department.clear()
        department.addAll(data)
        adapter.notifyDataSetChanged()
        hideProgressBar()
        swipeRefresh.isRefreshing = false
    }

    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private var departmentList: MutableList<DepartmentItem> = mutableListOf()
    private var department: MutableList<DepartmentItem> = mutableListOf()
    private lateinit var adapter: MyDepartmentItemRecyclerViewAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var presenter: DepartmentListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_department)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference.child("department").child("dpr")

        getDepartmentList()
        recyclerView = list_department
        adapter = MyDepartmentItemRecyclerViewAdapter(department, this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        swipeRefresh = swipe_refresh
        presenter = DepartmentListPresenter(this, this)
    }

    override fun onStart() {
        swipeRefresh.onRefresh {
            presenter.getDepartmentList(departmentList)
        }
        super.onStart()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_item, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> finish()
            R.id.add_item -> showAlertAddDepartment()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAlertAddDepartment() {
        var departmentName: EditText? = null
        var departmentDescription: EditText? = null
        alert ("Masukkan Nama Komisi"){
            customView{
                verticalLayout {
                    departmentName = editText {
                        hint = "Nama Komisi"
                    }.lparams(width = matchParent) {
                        margin = 5
                    }
                    departmentDescription = editText {
                        hint = "Deskripsi Komisi"
                        maxLines = 10
                    }.lparams(width = matchParent) {
                        margin = 5
                    }
                }
            }
            okButton {
                val key = myRef.push().key
                storeDepartment(key!!, "${departmentName!!.text}", "${departmentDescription!!.text}")
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    private fun alertUpdateDepartment(key: String, name: String, description: String) {
        alert("Edit Komisi") {
            var departmentName: EditText? = null
            var departmentDescription: EditText? = null
            customView {
                verticalLayout {
                    departmentName = editText(name) {
                        hint = "Nama Komisi"
                    }.lparams(width = matchParent) {
                        margin = 5
                    }
                    departmentDescription = editText(description) {
                        hint = "Deskripsi Komisi"
                        maxLines = 10
                    }.lparams(width = matchParent) {
                        margin = 5
                    }
                }
            }
            okButton {
                storeDepartment(key, "${departmentName?.text}",
                        "${departmentDescription?.text}")
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    private fun storeDepartment(key: String, departmentName: String, departmentDescription: String) {
        val dialog = progressDialog(message = "Mohon Tunggu", title = "Menambah Komisi")
        dialog.show()
        dialog.progress = 30

        dialog.progress = 60
        val department = DepartmentItem(key, departmentName, departmentDescription)

        myRef.child(key).setValue(department).addOnSuccessListener {
            dialog.progress = 100
            dialog.cancel()
            snackbar(root_view, "Komisi Ditambahkan")
        }
    }

    private fun getDepartmentList() {
        showProgressBar()
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                departmentList.clear()
                snapshot.children.mapNotNullTo(departmentList) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                Log.e("departmentList", "$departmentList")
                presenter.getDepartmentList(departmentList)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.addValueEventListener(departmentListListener)
    }
}
