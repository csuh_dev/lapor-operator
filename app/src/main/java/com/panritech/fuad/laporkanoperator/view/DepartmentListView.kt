package com.panritech.fuad.laporkanoperator.view

import com.panritech.fuad.laporkanoperator.model.DepartmentItem

interface DepartmentListView {
    fun showDepartmentList(data: List<DepartmentItem>)
    fun deleteDepartment(data: DepartmentItem)
    fun showDepartmentDescription(data: DepartmentItem)
}