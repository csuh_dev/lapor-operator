package com.panritech.fuad.laporkanoperator.presenter

import com.panritech.fuad.laporkanoperator.fragment.ReportItemFragment
import com.panritech.fuad.laporkanoperator.model.ReportItem

class ReportPresenter (private val reportView: ReportItemFragment){

    fun getReportList(data: List<ReportItem>){
        reportView.showProgressBar()
        reportView.showReportItem(data)
        reportView.hideProgressBar()
    }
}