package com.panritech.fuad.laporkanoperator.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.fragment.NewReportItemFragment
import com.panritech.fuad.laporkanoperator.model.ReportItem
import org.jetbrains.anko.find

class MyNewReportItemRecyclerViewAdapter(
        var items: MutableList<ReportItem>,
        private val mListener: NewReportItemFragment.OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<MyNewReportItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_reportitem_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val txtDepartment:TextView = view.find(R.id.txtDepartment)
        val txtDate:TextView = view.find(R.id.txtDate)
        val txtReportTitle:TextView = view.find(R.id.txtReportTitle)
        val txtReportDescription:TextView = view.find(R.id.txtReportDescription)
        val txtLocation:TextView = view.find(R.id.txtLocation)
        val txtReportStatus:TextView = view.find(R.id.txtReportStatus)

        fun bindItem(items: ReportItem){

            txtDepartment.text = items.reportDepartment
            txtDate.text = items.reportDate
            txtReportTitle.text = items.reportTitle
            txtReportDescription.text = items.reportDescription
            txtLocation.text = items.reportLocation
            txtReportStatus.text = items.reportStatus

            itemView.setOnClickListener {
                mListener?.onListFragmentInteraction(items)
            }
        }
    }
}
