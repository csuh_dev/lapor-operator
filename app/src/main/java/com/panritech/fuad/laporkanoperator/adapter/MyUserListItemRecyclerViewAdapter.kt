package com.panritech.fuad.laporkanoperator.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.fragment.UserListItemFragment.OnListFragmentInteractionListener
import com.panritech.fuad.laporkanoperator.model.UserItem
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick

class MyUserListItemRecyclerViewAdapter(
        var items: MutableList<UserItem>,
        private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<MyUserListItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_userlistitem_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val txtUsername: TextView = view.find(R.id.txt_user_name)
        val txtLocation: TextView = view.find(R.id.txt_user_location)
        val txtUserDepartment: TextView = view.find(R.id.txt_user_department)
        val btnDelteUser: ImageButton = view.find(R.id.btn_delete_user)


        fun bindItem(items: UserItem){

            txtUsername.text = items.name
            txtLocation.text = items.location
            txtUserDepartment.text = items.department

            btnDelteUser.onClick {
                mListener?.onBtnDeleteInteraction(items)
            }

            itemView.apply {
                onClick {
                    mListener?.onListFragmentInteraction(items)
                }
            }
        }
    }
}
