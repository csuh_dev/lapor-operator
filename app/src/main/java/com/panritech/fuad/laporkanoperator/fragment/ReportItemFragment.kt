package com.panritech.fuad.laporkanoperator.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import com.google.firebase.database.*
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.adapter.MyReportItemRecyclerViewAdapter
import com.panritech.fuad.laporkanoperator.model.DepartmentItem
import com.panritech.fuad.laporkanoperator.model.ReportItem
import com.panritech.fuad.laporkanoperator.presenter.ReportPresenter
import com.panritech.fuad.laporkanoperator.view.ProgressBarView
import com.panritech.fuad.laporkanoperator.view.ReportView

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ReportItemFragment.OnListFragmentInteractionListener] interface.
 */
class ReportItemFragment : Fragment(), ReportView, ProgressBarView {
    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showReportItem(data: List<ReportItem>) {
        //report.sortBy {it.reportStatus}
        Log.e("ReportItemFragment:data", "$report")
        report.clear()
        report.addAll(data)
        filterBy(department, status)
        adapter.notifyDataSetChanged()
        hideProgressBar()
        swipeRefresh.isRefreshing = false
    }

    // TODO: Customize parameters
    private var userID = ""

    private var departmentList: HashMap<String, String> = hashMapOf()
    private var departmentSpinnerList: ArrayList<String> = arrayListOf()
    private var reportList: MutableList<ReportItem> = mutableListOf()
    private var report: MutableList<ReportItem> = mutableListOf()
    private var listener: OnListFragmentInteractionListener? = null
    private var status: String = ""
    private var department: String = ""
    private lateinit var myRef: DatabaseReference
    private lateinit var reportPresenter: ReportPresenter
    private lateinit var adapter: MyReportItemRecyclerViewAdapter
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var statusSpinner: Spinner
    private lateinit var departmentSpinner: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            userID = it.getString(ARG_USER_ID)
        }
        setHasOptionsMenu(true)
        myRef = FirebaseDatabase.getInstance().reference
        adapter = MyReportItemRecyclerViewAdapter(reportList, listener)
        reportPresenter = ReportPresenter(this)
        getDepartment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reportitem, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list_report)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        progressBar = view.findViewById(R.id.progressBar)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        departmentSpinner = view.findViewById(R.id.spinner_department)
        statusSpinner = view.findViewById(R.id.spinner_status)

        statusSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                status = parent?.getItemAtPosition(position).toString()
                if (position == 0)
                    status = ""
                filterBy(department, status)
            }
        }

        departmentSpinnerList.add("Semua Komisi dan SKPD")

        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, departmentSpinnerList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        departmentSpinner.adapter = adapter
        departmentSpinner.getItemAtPosition(0)
        departmentSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                department = parent?.getItemAtPosition(position).toString()
                if (position == 0)
                    department = ""
                filterBy(department, status)
            }
        }

        reportPresenter.getReportList(reportList)
        getReportData()

        swipeRefresh.setOnRefreshListener {
            getReportData()
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun getReportData() {
        val reportEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                reportList.clear()
                snapshot.children.mapNotNullTo(reportList) {
                    it.getValue<ReportItem>(ReportItem::class.java)
                }
                reportList.forEach {
                    if (it.reportDepartment != "") {
                        it.reportDepartment = departmentList[it.reportDepartment]
                    }
                }
                reportPresenter.getReportList(reportList)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("report").addValueEventListener(reportEventListener)
    }

    private fun getDepartment() {
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val data: MutableList<DepartmentItem> = mutableListOf()
                snapshot.children.mapNotNullTo(data) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                for (item in data) {
                    departmentList[item.key] = item.name
                    departmentSpinnerList.add(item.name)
                }
                Log.e("departmentList", "$data")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("department/dpr").addValueEventListener(departmentListListener)
        myRef.child("department/skpd").addValueEventListener(departmentListListener)
    }

    fun filterBy(department: String, status: String) {
        adapter.items = report.asSequence().filter {
            it.reportDepartment!!.contains(department) && it.reportStatus!!.contains(status) && !it.reportStatus!!.contains("new")
        }.toMutableList()
        adapter.notifyDataSetChanged()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: ReportItem)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_USER_ID = "user-id"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(userID: String) =
                ReportItemFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_USER_ID, userID)
                    }
                }
    }
}
