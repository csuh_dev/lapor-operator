package com.panritech.fuad.laporkanoperator.view

import com.panritech.fuad.laporkanoperator.model.ReportItem

interface ReportView {
    fun showReportItem(data: List<ReportItem>)
}