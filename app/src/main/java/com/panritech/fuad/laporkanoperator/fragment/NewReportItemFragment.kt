package com.panritech.fuad.laporkanoperator.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.google.firebase.database.*
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.adapter.MyNewReportItemRecyclerViewAdapter

import com.panritech.fuad.laporkanoperator.model.ReportItem
import com.panritech.fuad.laporkanoperator.presenter.NewReportPresenter
import com.panritech.fuad.laporkanoperator.view.NewReportView
import com.panritech.fuad.laporkanoperator.view.ProgressBarView
import org.jetbrains.anko.support.v4.toast

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [NewReportItemFragment.OnListFragmentInteractionListener] interface.
 */
class NewReportItemFragment : Fragment(), NewReportView, ProgressBarView {

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun showReportItem(data: List<ReportItem>) {
        report.clear()
        report.addAll(data)
        filterBy()
        adapter.notifyDataSetChanged()
        hideProgressBar()
        swipeRefresh.isRefreshing = false
    }

    // TODO: Customize parameters
    private var userID = ""

    private var reportList: MutableList<ReportItem> = mutableListOf()
    private var report: MutableList<ReportItem> = mutableListOf()
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var myRef: DatabaseReference
    private lateinit var reportPresenter: NewReportPresenter
    private lateinit var adapter: MyNewReportItemRecyclerViewAdapter
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var spinnerContainer: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            userID = it.getString(ReportItemFragment.ARG_USER_ID)
        }
        myRef = FirebaseDatabase.getInstance().reference
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_reportitem, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list_report)

        recyclerView.layoutManager = LinearLayoutManager(context)
        Log.e("data", report.toString())
        adapter = MyNewReportItemRecyclerViewAdapter(report, listener)
        recyclerView.adapter = adapter

        progressBar = view.findViewById(R.id.progressBar)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        spinnerContainer = view.findViewById(R.id.spinner_container)

        spinnerContainer.visibility = View.GONE
        reportPresenter = NewReportPresenter(this)
        getReportData()

        swipeRefresh.setOnRefreshListener {
            getReportData()
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NewReportItemFragment.OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun filterBy(){
        adapter.items = report.asSequence().filter{
            it.reportStatus!!.contains("new")
        }.toMutableList()
        adapter.notifyDataSetChanged()
    }

    private fun getReportData() {
        val reportEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                reportList.clear()
                snapshot.children.mapNotNullTo(reportList) {
                    it.getValue<ReportItem>(ReportItem::class.java)
                }
                Log.e("loadPost:Cancelled", reportList.toString())
                reportPresenter.getReportList(reportList)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
                toast("Gagal Mengambil Data")
            }
        }
        myRef.child("report").addValueEventListener(reportEventListener)
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: ReportItem)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_USER_ID = "user-id"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(userID: String) =
                ReportItemFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_USER_ID, userID)
                    }
                }
    }
}
