package com.panritech.fuad.laporkanoperator.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.database.*
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.adapter.MyUserListItemRecyclerViewAdapter
import com.panritech.fuad.laporkanoperator.model.UserItem
import com.panritech.fuad.laporkanoperator.presenter.UserItemPresenter
import com.panritech.fuad.laporkanoperator.view.ProgressBarView
import com.panritech.fuad.laporkanoperator.view.UserItemView
import kotlinx.android.synthetic.main.fragment_userlistitem.view.*
import org.jetbrains.anko.support.v4.onRefresh

private const val ARG_LEVEL = "userLevel"

class UserListItemFragment : Fragment(), UserItemView, ProgressBarView {

    private var listUser: MutableList<UserItem> = mutableListOf()
    private var users: MutableList<UserItem> = mutableListOf()
    private var userLevel: String = ""
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var presenter: UserItemPresenter
    private lateinit var adapter: MyUserListItemRecyclerViewAdapter
    private lateinit var myRef: DatabaseReference
    private var listener: OnListFragmentInteractionListener? = null

    override fun showUsersItem(data: List<UserItem>) {
        users.clear()
        users.addAll(data)
        Log.e("Data ", "$users")
        filterBy(userLevel)
        hideProgressBar()
        swipeRefresh.isRefreshing = false
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            userLevel = it.getString(ARG_LEVEL)
        }
        myRef = FirebaseDatabase.getInstance().reference
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_userlistitem, container, false)

        val recycleView = view.findViewById<RecyclerView>(R.id.list_users)

        recycleView.layoutManager = LinearLayoutManager(context)
        adapter = MyUserListItemRecyclerViewAdapter(users, listener)
        recycleView.adapter = adapter
        swipeRefresh = view.swipe_refresh
        progressBar = view.progressBar

        getListUser()
        presenter = UserItemPresenter(this, this)
        presenter.getUserList(listUser)

        swipeRefresh.onRefresh {
            //(activity as ListUserActivity).getListUser()
            getListUser()
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        if (listUser.isEmpty()) {
            //(activity as ListUserActivity).getListUser()
            showProgressBar()
        } else {
            presenter.getUserList(listUser)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun getListUser() {
        showProgressBar()
        val userListListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Log.e("loadUser:onCancelled", "${error.toException()}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                listUser.clear()
                snapshot.children.mapNotNullTo(listUser) {
                    it.getValue<UserItem>(UserItem::class.java)
                }
                presenter.getUserList(listUser)
            }
        }
        myRef.child("users").addValueEventListener(userListListener)
    }

    private fun filterBy(userLevel: String) {
        adapter.items = listUser.asSequence().filter {
            it.type.contains(userLevel) && it.status.contentEquals("active")
        }.toMutableList()
        adapter.notifyDataSetChanged()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: UserItem)
        fun onBtnDeleteInteraction(item: UserItem)
    }

    companion object {
        @JvmStatic
        fun newInstance(userType: String) =
                UserListItemFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_LEVEL, userType)
                    }
                }
    }
}
