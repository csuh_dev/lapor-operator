package com.panritech.fuad.laporkanoperator.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.model.DepartmentItem
import com.panritech.fuad.laporkanoperator.view.DepartmentListView
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick

class MyDepartmentItemRecyclerViewAdapter(
        private val items: MutableList<DepartmentItem>,
        private val departmentListView: DepartmentListView)
    : RecyclerView.Adapter<MyDepartmentItemRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_department_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        val txtDepartment: TextView = view.find(R.id.txt_department_name)
        private val btnDeleteDepartment: ImageButton = view.find(R.id.btn_delete_department)

        fun bindItem(items: DepartmentItem){
            txtDepartment.text = items.name
            btnDeleteDepartment.onClick {
                departmentListView.deleteDepartment(items)
            }
            view.onClick {
                departmentListView.showDepartmentDescription(items)
            }
        }
    }
}