package com.panritech.fuad.laporkanoperator.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.activity.AddOperatorActivity
import com.panritech.fuad.laporkanoperator.activity.DepartmentActivity
import com.panritech.fuad.laporkanoperator.activity.ListUserActivity
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.startActivity

class OtherInfoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var btnListUser: TextView
    private lateinit var btnListDepartment: TextView
    private lateinit var btnAddOperator: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_other_info, container, false)

        btnListUser = view.findViewById(R.id.btn_list_user)
        btnListDepartment = view.findViewById(R.id.btn_list_department)
        btnAddOperator = view.findViewById(R.id.btn_add_operator)

        btnListUser.onClick {
            startActivity<ListUserActivity>()
        }

        btnListDepartment.onClick {
            startActivity<DepartmentActivity>()
        }
        btnAddOperator.onClick {
            startActivity<AddOperatorActivity>()
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        //fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                OtherInfoFragment()
    }
}
