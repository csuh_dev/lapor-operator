package com.panritech.fuad.laporkanoperator.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.panritech.fuad.laporkanoperator.fragment.NewReportItemFragment
import com.panritech.fuad.laporkanoperator.fragment.ReportItemFragment

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                ReportItemFragment()
            }
            else -> {
                return NewReportItemFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "List Laporan"
            else -> {
                return "Laporan Baru"
            }
        }
    }
}