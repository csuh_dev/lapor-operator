package com.panritech.fuad.laporkanoperator.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.fragment.UserListItemFragment
import com.panritech.fuad.laporkanoperator.model.UserItem
import kotlinx.android.synthetic.main.activity_list_user.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick

class ListUserActivity : AppCompatActivity(), UserListItemFragment.OnListFragmentInteractionListener {
    override fun onBtnDeleteInteraction(item: UserItem) {
        alert("Ingin Menghapus User ${item.name}?") {
            yesButton {
                deleteUser(item)
                it.cancel()
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    override fun onListFragmentInteraction(item: UserItem) {

    }

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var menuItem: Menu? = null
    private lateinit var myRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_user)
        setSupportActionBar(toolbar)

        myRef = FirebaseDatabase.getInstance().reference

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        container.adapter = mSectionsPagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_item,menu)
        menuItem = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home -> finish()
            R.id.add_item -> addUserDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addUserDialog(){
        val dialogView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_add_user, null)
        val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)
        // set message of alert dialog
        dialogBuilder
                .setMessage("Tambah User")
                .setNegativeButton("Batal") { dialog, _ ->
                    dialog.cancel()
                }

        val btnAddDpr = dialogView.findViewById<Button>(R.id.btn_add_dpr)
        val btnAddSkpd = dialogView.findViewById<Button>(R.id.btn_add_skpd)

        val alert = dialogBuilder.create()
        alert.show()

        btnAddDpr.onClick {
            startActivity<AddUserActivity>("userType" to "dpr")
            alert.cancel()
        }

        btnAddSkpd.onClick {
            startActivity<AddUserActivity>("userType" to "skpd")
            alert.cancel()
        }
    }

    private fun deleteUser(item: UserItem) {
        item.status = "notActive"
        val dialog = indeterminateProgressDialog("Mohon Tunggu ", "Menghapus Data")
        dialog.show()
        myRef.child("users/${item.uuid}").setValue(item).addOnSuccessListener {
            snackbar(main_content, "User Dihapus")
        }

        dialog.cancel()
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return when(position){
                0 -> UserListItemFragment.newInstance("masyarakat")
                1 -> UserListItemFragment.newInstance("dpr")
                else -> UserListItemFragment.newInstance("skpd")
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }
}
