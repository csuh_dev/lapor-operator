package com.panritech.fuad.laporkanoperator.activity

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.ImageView
import com.panritech.fuad.laporkanoperator.R
import kotlinx.android.synthetic.main.activity_add_report.*
import org.jetbrains.anko.toast

class AddReportActivity : AppCompatActivity() {

    private val REQUEST_IMAGE_CAPTURE = 1
    private lateinit var imageReport: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_report)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        imageReport = findViewById(R.id.imageReport)

        btnSendReport.setOnClickListener {
            finish()
            toast("Aduan Dikirim")
        }

        textBtnCamera.setOnClickListener {
            cameraAlertDialog()
        }

        textBtnDelete.setOnClickListener {
            imageReport.setImageBitmap(null)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun cameraAlertDialog(){
        val dialogView = LayoutInflater.from(this).inflate(R.layout.layout_dialog_camera, null)

        val dialogBuilder = AlertDialog.Builder(this).setView(dialogView)

        // set message of alert dialog
        dialogBuilder.setMessage("Ambil Gambar Dengan Posisi HP Mendatar/Tidur")
                .setCancelable(false)
                .setPositiveButton("Ambil Gambar") { _, _ ->
                    dispatchTakePictureIntent()
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
        val alert = dialogBuilder.create()
        alert.setTitle("Perhatian")
        alert.show()
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data.extras.get("data") as Bitmap
            imageReport.setImageBitmap(imageBitmap)
        }
    }
}
