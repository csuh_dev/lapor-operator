package com.panritech.fuad.laporkanoperator.presenter

import com.panritech.fuad.laporkanoperator.model.UserItem
import com.panritech.fuad.laporkanoperator.view.ProgressBarView
import com.panritech.fuad.laporkanoperator.view.UserItemView

class UserItemPresenter(private val userItemView: UserItemView, private val progressBarView: ProgressBarView) {

    fun getUserList(data: List<UserItem>){
        progressBarView.showProgressBar()
        progressBarView.hideProgressBar()
        userItemView.showUsersItem(data)
    }
}