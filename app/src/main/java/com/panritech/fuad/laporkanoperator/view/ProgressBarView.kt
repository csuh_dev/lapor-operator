package com.panritech.fuad.laporkanoperator.view

interface ProgressBarView {
    fun showProgressBar()
    fun hideProgressBar()
}