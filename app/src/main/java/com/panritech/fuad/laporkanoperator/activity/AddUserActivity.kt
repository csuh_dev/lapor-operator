package com.panritech.fuad.laporkanoperator.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.model.DepartmentItem
import com.panritech.fuad.laporkanoperator.model.UserItem
import kotlinx.android.synthetic.main.activity_add_user.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.*

class AddUserActivity : AppCompatActivity() {

    private var departmentList: MutableList<DepartmentItem> = mutableListOf()
    private var department: ArrayList<String> = arrayListOf()
    private var userItem: MutableList<UserItem> = mutableListOf()
    private lateinit var auth: FirebaseAuth
    private lateinit var fireDatabase: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    private lateinit var spinner: Spinner
    private lateinit var operatorValidationPassword: EditText
    private var userDepartment: String = ""
    private var operatorEmail: String = ""
    private var operatorPass: String = ""
    private var userType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        auth = FirebaseAuth.getInstance()
        fireDatabase = FirebaseDatabase.getInstance()
        myRef = fireDatabase.reference

        operatorEmail = auth.currentUser?.email.toString()

        userType = intent.getStringExtra("userType")
        spinner = spinner_department

        if (userType == "dpr") {
            getDepartmentList()
            txt_skpd_department.visibility = View.GONE
        } else {
            spinner.visibility = View.GONE
            radio_level.visibility = View.GONE
        }
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userDepartment = department[position]
            }
        }

        btn_add_user.onClick {
            validateForm()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setSpinner() {
        department.clear()
        Log.e("department:List", "$departmentList")
        Log.e("department", "$department")
        for (item in departmentList) {
            department.add(item.name)
        }
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, department)
        spinner.adapter = spinnerAdapter
        spinner.setSelection(0)
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            snackbar(root_view, "Berhasil Menambahkan Akun")
            resetView()
        } else {
            snackbar(root_view, "Gagal Menambah Akun")
        }
    }

    private fun resetView() {
        img_profile_pic.imageResource = R.drawable.ic_boy
        txt_user_name.setText("")
        txt_email.setText("")
        txt_password.setText("")
        txt_confirm_password.setText("")
        txt_phone_number.setText("")
        txt_alamat.setText("")
        txt_skpd_department.setText("")
    }

    private fun validateForm() {
        val dialog = progressDialog("Verifikasi Form")
        dialog.show()
        dialog.progress = 50
        val userName = txt_user_name.text
        val userEmail = txt_email.text
        val userPassword = txt_password.text
        val confirmPassword = txt_confirm_password.text
        val phoneNumber = txt_phone_number.text
        val location = txt_alamat.text
        val gender = if (radio_btn_man.isChecked)
            "laki-laki"
        else
            "wanita"
        var level = ""
        if (userType == "dpr") {
            level = if (radio_btn_leader.isChecked)
                "ketua"
            else
                "anggota"
        } else {
            userDepartment = txt_skpd_department.text.toString()
        }
        dialog.progress = 80
        if (userName.isNotEmpty() && userEmail.isNotEmpty() && userPassword.isNotEmpty() && phoneNumber.isNotEmpty() && location.isNotEmpty()) {
            if ("$confirmPassword" == "$userPassword" && userPassword.length >= 6) {
                userItem.clear()
                val user = UserItem("", "$userEmail", "$userName", gender, "$phoneNumber"
                        , userDepartment, "$location", userType, level, "active")
                userItem.add(user)
                alertStoreData(user, "$userPassword")
            } else {
                txt_password.backgroundResource = R.drawable.card_stroke_red
                txt_confirm_password.backgroundResource = R.drawable.card_stroke_red
                snackbar(root_view, "Periksa kembali password anda, minimal 6 character")
            }
        } else {
            snackbar(root_view, "Isi Semua Form")
        }
        dialog.progress = 100
        dialog.cancel()
    }

    private fun alertStoreData(user: UserItem, password: String) {
        alert("Yakin data sudah benar?", "Daftarkan akun") {
            yesButton {
                alertOperatorValidation(user.email, password)
                it.cancel()
            }
            noButton {
                it.cancel()
            }
        }.show()
    }

    private fun alertOperatorValidation(email: String, password: String) {
        if (operatorPass != "")
            verifyOperator(email, password, operatorPass)
        else {
            alert("Silahkan Memasukkan Password Anda", "Validasi Operator") {
                customView {
                    verticalLayout {
                        operatorValidationPassword = editText {
                            hint = "Password"
                            inputType = TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD
                        }.lparams(width = matchParent) {
                            leftMargin = dip(10)
                            rightMargin = dip(10)
                        }
                    }
                }
                okButton {
                    operatorPass = operatorValidationPassword.text.toString()
                    verifyOperator(email, password, operatorPass)
                }
                cancelButton {
                    finishAffinity()
                    FirebaseAuth.getInstance().signOut()
                    startActivity<LoginActivity>()
                    toast("Silahkan Login Kembali")
                }
            }.show()
        }
    }

    private fun verifyOperator(email: String, password: String, operatorValidation: String) {
        val dialog = progressDialog(message = "Mohon Tunggu", title = "Memverifikasi Akun")
        dialog.show()
        dialog.progress = 30
        Log.e("validation", operatorEmail)
        auth.signInWithEmailAndPassword(operatorEmail, operatorValidation)
                .addOnCompleteListener(this) { task ->
                    dialog.progress = 80
                    if (task.isSuccessful) {
                        addUser(email, password, operatorValidation)
                        dialog.progress = 100
                    } else {
                        toast("Anda Gagal Divalidasi")
                        alertOperatorValidation(email, password)
                    }
                }
        dialog.cancel()
    }

    private fun addUser(email: String, password: String, operatorValidation: String) {
        val dialog = progressDialog(message = "Mohon Tunggu", title = "Mendaftarkan Akun")
        dialog.show()
        dialog.progress = 50
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success")
                        val user = auth.currentUser
                        userItem[0].uuid = user!!.uid

                        addUserDetail(userItem)
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        toast("Authentication failed.")
                        updateUI(null)
                    }
                    FirebaseAuth.getInstance().signOut()
                }

        dialog.progress = 80
        auth.signInWithEmailAndPassword(operatorEmail, operatorValidation)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        dialog.progress = 100
                        dialog.cancel()
                    }
                }
    }

    private fun addUserDetail(userItem: MutableList<UserItem>) {
        val key = userItem[0].uuid
        val skpd = DepartmentItem(key, userDepartment, "")
        myRef.child("department/skpd/$key").setValue(skpd)
        myRef.child("users/$key").setValue(userItem[0]).addOnSuccessListener {
            Log.i("firebase:database", "Success Write Data")
        }
    }

    private fun getDepartmentList() {
        val dialog = progressDialog(message = "Mohon Tunggu Sebentar")
        dialog.show()
        dialog.progress = 30
        val departmentListListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                dialog.progress = 70
                departmentList.clear()
                snapshot.children.mapNotNullTo(departmentList) {
                    it.getValue<DepartmentItem>(DepartmentItem::class.java)
                }
                dialog.progress = 100

                setSpinner()

                dialog.cancel()
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("loadPost:Cancelled", "${error.toException()}")
            }
        }
        myRef.child("department").child("dpr").addValueEventListener(departmentListListener)
    }

    companion object {
        private const val TAG = "EmailPassword"
    }
}
