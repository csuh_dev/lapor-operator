package com.panritech.fuad.laporkanoperator.view

import com.panritech.fuad.laporkanoperator.model.UserItem

interface UserItemView {
    fun showUsersItem(data: List<UserItem>)
}