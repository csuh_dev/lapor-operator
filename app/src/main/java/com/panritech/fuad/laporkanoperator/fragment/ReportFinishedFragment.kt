package com.panritech.fuad.laporkanoperator.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.panritech.fuad.laporkanoperator.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_FINISHED_DATE = "finishedDate"
private const val ARG_FINISHED_DESCRIPTION = "finishedDescription"
private const val ARG_FINISHED_IMG = "finishedImg"

class ReportFinishedFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var finishedDate: String? = null
    private var finishedDescription: String? = null
    private var finishedImg: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            finishedDate = it.getString(ARG_FINISHED_DATE)
            finishedDescription = it.getString(ARG_FINISHED_DESCRIPTION)
            finishedImg = it.getString(ARG_FINISHED_IMG)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_process, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ReportFinishedFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(finishedDate: String, finishedDescription: String, finishedImg: String) =
                ReportFinishedFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_FINISHED_DATE, finishedDate)
                        putString(ARG_FINISHED_DESCRIPTION, finishedDescription)
                        putString(ARG_FINISHED_IMG, finishedImg)
                    }
                }
    }
}
