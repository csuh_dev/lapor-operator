package com.panritech.fuad.laporkanoperator.model

class UserTable(val userid: String, val userEmail: String, val userName: String) {
    companion object {
        const val TABLE_USER: String = "TABLE_USER"
        const val USER_ID: String = "USER_ID"
        const val USER_EMAIL: String = "USER_EMAIL"
        const val USER_FULLNAME: String = "USER_FULLNAME"
    }
}