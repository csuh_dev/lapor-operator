package com.panritech.fuad.laporkanoperator.fragment

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.panritech.fuad.laporkanoperator.R
import com.panritech.fuad.laporkanoperator.adapter.MyPagerAdapter

class ReportFragment : Fragment() {

    private lateinit var viewPager: ViewPager
    private lateinit var tabs: TabLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report, container, false)
        val fragmentAdapter = MyPagerAdapter(childFragmentManager)

        viewPager = view.findViewById(R.id.viewpager_main)
        tabs = view.findViewById(R.id.tabs_main)
        viewPager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewPager)

        return view
    }

    companion object {
        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance() = ReportFragment()
    }
}
