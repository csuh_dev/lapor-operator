package com.panritech.fuad.laporkanoperator.view

import com.panritech.fuad.laporkanoperator.model.ReportItem

interface NewReportView {
    fun showReportItem(data: List<ReportItem>)
}